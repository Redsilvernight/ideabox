function viewBoxEdit()
{
    var url ="box/rename";
    var editElement = document.getElementById("boxEdit");

    axios.get(url).then(function(response) { 
        editElement.innerHTML = response.data;
    })
}

function removeBox()
{
    var url ="box/delete";
    axios.get(url).then(function(response) { 
        window.location.reload();
    })
}