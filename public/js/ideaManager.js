
function removeIdea(event) {
    if(confirm("Etes-vous sûr de vouloir supprimer l'idée ?"))
    {
        var url = "/idea/remove/" + event;
    
        axios.get(url).then(function(response) {
            window.location.reload();
        });
    }
    else {
        return;
    }
}

function printIdea(event) {
    if(confirm("Etes-vous sûr de vouloir imprimer l'idée ?"))
    {
        var url = "/idea/print/" + event;
    
        axios.get(url).then(function(response) {
            window.location.reload();
        });
    } else {
        return;
    }
}