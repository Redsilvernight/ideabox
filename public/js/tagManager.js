function removeTag(event) {
    if(confirm("Etes-vous sûr de vouloir supprimer la catégorie ? les idées associées le seront aussi."))
    {
        var url = "/tag/remove/" + event;
    
        axios.get(url).then(function(response) {
            window.location.reload();
        });
    } else {
        return;
    }
}

function addTag()
{
    var url = "/tag/add"
    var addElement = document.getElementById("addTagContent");
    
    document.getElementById("addTagBtn").style.display = "none";

    axios.get(url).then(function(response) {
        addElement.innerHTML = response.data;
    });
}

function randomTag(idTag)
{
    var url = "/idea/random/" + idTag;
    var randomIdeaElement = document.getElementById("randomTagContent_" + idTag);

    axios.get(url).then(function(response) {
        randomIdeaElement.innerHTML = response.data;
    });
}

function closeRandomTag(idTag)
{
    var randomIdeaElement = document.getElementById("randomTagContent_" + idTag);

    randomIdeaElement.innerHTML = "";
    return;
}
