var defaultPrintCheck = document.getElementById('defaultPrint');

defaultPrintCheck.addEventListener('change', function(){
    setDefaultPrint(defaultPrintCheck.checked?1:0)
})

function setDefaultPrint(value)
{
    var url = "/box/defaultPrint/" + value;

    axios.get(url).then(function(response) {
    });
}

function getUtilsPath()
{
    const inputElement = document.getElementById("pythonUtils")
    const file = inputElement.files[0]

    if (!file) return
    const reader = new FileReader()
    reader.onload = (e) => {
        // e.target points to the reader
        const textContent = e.target.result
        saveIpPrinter(textContent)
    }
    reader.onerror = (e) => {
        const error = e.target.error
    }
    reader.readAsText(file, 'utf-8')
}

function saveIpPrinter(textContent)
{
    var url = '/setting/link/' + textContent

    axios.get(url).then(function(response) { 
        window.location.reload();
    })    
}