function viewInvite()
{
    var url = "/setting/invite";
    var inviteElement = document.getElementById('inviteUserContent');

    axios.get(url).then(function(response) { 
        inviteElement.innerHTML = response.data;
    }) 
}

function removeOwner(owner)
{
    var url = "/setting/removeOwner/" + owner;
    if(confirm("Etes-vous sûr de vouloir supprimer cet utilisateur de la boîte ? Il se verra créer une nouvelle boîte."))
    {
        axios.get(url).then(function(response) { 
        }) 
    } else {
        return;
    }
}

function removeAccount(account)
{
    var url = "/setting/remove/" + account
    if(confirm("Etes-vous sûr de vouloir supprimer votre compte ? Cela pourra avoir une incidence sur votre boîte.")) {
        axios.get(url).then(function(response) { 
            window.location.reload();
        }) 
    } else {
        return;
    }
}