/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

loadIdeaContent();

var tagView = document.getElementById("boxTag");
var ideaView = document.getElementById("boxIdea");

tagView.addEventListener("click", function(){
    viewTag();
});
ideaView.addEventListener("click", function(){
    viewIdea();
});

function viewTag() {
    ideaView.classList.remove("focus");
    ideaView.classList.add("notFocus");
    tagView.classList.add("focus");
    tagView.classList.remove("notFocus");
    loadTagContent();
}

function viewIdea() {
    tagView.classList.remove("focus");
    tagView.classList.add("notFocus");
    ideaView.classList.remove("notFocus");
    ideaView.classList.add("focus");
    loadIdeaContent();
}

function loadIdeaContent()
{
    var url = "/idea/list";
    var viewContent = document.getElementById("boxContent");

    axios.get(url).then(function(response) {
        viewContent.innerHTML = response.data;
    });
}

function loadTagContent() {
    var url = "/tag/list"
    var viewContent = document.getElementById("boxContent");

    axios.get(url).then(function(response) {
        viewContent.innerHTML = response.data;
    });
}