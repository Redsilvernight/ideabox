<?php

namespace App\Form;

use App\Entity\Idea;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IdeaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('idea_name')
            ->add('idea_tag_id', EntityType::class, [
                'class' => Tag::class,
                'choices' => $options['available_tags'],
                'choice_label' => 'tag_name',
                'placeholder' => "Choisis une catégorie"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'available_tags' => false,
            'data_class' => Idea::class,
        ]);
    }
}
