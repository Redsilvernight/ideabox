<?php

namespace App\Entity;

use App\Repository\IdeaBoxRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IdeaBoxRepository::class)]
class IdeaBox
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $box_name = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $box_created_at = null;

    #[ORM\OneToMany(targetEntity: Tag::class, mappedBy: 'tag_box_id', orphanRemoval: true)]
    private Collection $tags;

    #[ORM\OneToMany(targetEntity: User::class, mappedBy:'ideaBox', orphanRemoval: false)]
    private Collection $box_owner_id;

    #[ORM\OneToMany(targetEntity: Idea::class, mappedBy: 'idea_box_id', orphanRemoval: true)]
    private Collection $ideas;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $printerIp = null;

    #[ORM\Column]
    private ?bool $isPrintable = null;

    #[ORM\Column(nullable: true)]
    private ?bool $defaultPrint = null;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->box_owner_id = new ArrayCollection();
        $this->ideas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBoxName(): ?string
    {
        return $this->box_name;
    }

    public function setBoxName(string $box_name): static
    {
        $this->box_name = $box_name;

        return $this;
    }

    public function getBoxCreatedAt(): ?\DateTimeImmutable
    {
        return $this->box_created_at;
    }

    public function setBoxCreatedAt(\DateTimeImmutable $box_created_at): static
    {
        $this->box_created_at = $box_created_at;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->setTagBoxId($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        if ($this->tags->removeElement($tag)) {
            // set the owning side to null (unless already changed)
            if ($tag->getTagBoxId() === $this) {
                $tag->setTagBoxId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getBoxOwnerId(): Collection
    {
        return $this->box_owner_id;
    }

    public function addBoxOwnerId(User $boxOwnerId): static
    {
        if (!$this->box_owner_id->contains($boxOwnerId)) {
            $this->box_owner_id->add($boxOwnerId);
            $boxOwnerId->setIdeaBox($this);
        }

        return $this;
    }

    public function removeBoxOwnerId(User $boxOwnerId): static
    {
        if ($this->box_owner_id->removeElement($boxOwnerId)) {
            // set the owning side to null (unless already changed)
            if ($boxOwnerId->getIdeaBox() === $this) {
                $boxOwnerId->setIdeaBox(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Idea>
     */
    public function getIdeas(): Collection
    {
        return $this->ideas;
    }

    public function addIdea(Idea $idea): static
    {
        if (!$this->ideas->contains($idea)) {
            $this->ideas->add($idea);
            $idea->setIdeaBoxId($this);
        }

        return $this;
    }

    public function removeIdea(Idea $idea): static
    {
        if ($this->ideas->removeElement($idea)) {
            // set the owning side to null (unless already changed)
            if ($idea->getIdeaBoxId() === $this) {
                $idea->setIdeaBoxId(null);
            }
        }

        return $this;
    }

    public function getPrinterIp(): ?string
    {
        return $this->printerIp;
    }

    public function setPrinterIp(?string $printerIp): static
    {
        $this->printerIp = $printerIp;

        return $this;
    }

    public function isIsPrintable(): ?bool
    {
        return $this->isPrintable;
    }

    public function setIsPrintable(bool $isPrintable): static
    {
        $this->isPrintable = $isPrintable;

        return $this;
    }

    public function isDefaultPrint(): ?bool
    {
        return $this->defaultPrint;
    }

    public function setDefaultPrint(?bool $defaultPrint): static
    {
        $this->defaultPrint = $defaultPrint;

        return $this;
    }
}
