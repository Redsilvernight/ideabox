<?php

namespace App\Entity;

use App\Repository\IdeaRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IdeaRepository::class)]
class Idea
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $idea_name = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $idea_created_at = null;

    #[ORM\ManyToOne(inversedBy: 'ideas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tag $idea_tag_id = null;

    #[ORM\ManyToOne(inversedBy: 'ideas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?IdeaBox $idea_box_id = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdeaName(): ?string
    {
        return $this->idea_name;
    }

    public function setIdeaName(string $idea_name): static
    {
        $this->idea_name = $idea_name;

        return $this;
    }

    public function getIdeaCreatedAt(): ?\DateTimeImmutable
    {
        return $this->idea_created_at;
    }

    public function setIdeaCreatedAt(\DateTimeImmutable $idea_created_at): static
    {
        $this->idea_created_at = $idea_created_at;

        return $this;
    }

    public function getIdeaTagId(): ?Tag
    {
        return $this->idea_tag_id;
    }

    public function setIdeaTagId(?Tag $idea_tag_id): static
    {
        $this->idea_tag_id = $idea_tag_id;

        return $this;
    }

    public function getIdeaBoxId(): ?IdeaBox
    {
        return $this->idea_box_id;
    }

    public function setIdeaBoxId(?IdeaBox $idea_box_id): static
    {
        $this->idea_box_id = $idea_box_id;

        return $this;
    }
}
