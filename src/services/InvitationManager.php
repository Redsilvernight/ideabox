<?php

namespace App\services;

use App\Entity\Invitation;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

class InvitationManager
{
    public function __construct(private EmailVerifier $emailVerifier)
    {

    }

    public function sendEmailInvite(Invitation $invitation)
    {
        $this->emailVerifier->sendEmailInvitation(
            (new TemplatedEmail())
                ->from(new Address('redsilvernight@gmail.com', 'Register Bot'))
                ->to($invitation->getEmail())
                ->subject('Invitation à rejoindre une boîte')
                ->htmlTemplate('registration/invitation_email.html.twig'),
            $invitation
        );

        return;
    }
}
