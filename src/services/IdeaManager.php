<?php

namespace App\services;

use App\Entity\Idea;
use App\Entity\User;
use App\Repository\IdeaRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

class IdeaManager
{
    private $manager;
    private $repository;

    public function __construct(EntityManagerInterface $manager, IdeaRepository $ideaRepository)
    {
        $this->manager = $manager;
        $this->repository = $ideaRepository;
    }

    public function addIdea(Idea $idea, User $user)
    {
        $idea->setIdeaCreatedAt(new DateTimeImmutable('now'))
            ->setIdeaBoxId($user->getIdeaBox());

        $this->manager->persist($idea);
        $this->manager->flush();
            
    }

    public function listIdea(User $user)
    {
        return $this->repository->findBy(["idea_box_id" => $user->getIdeaBox()->getId()]);
    }

    public function removeIdea(Idea $idea)
    {
        $this->manager->remove($idea);
        $this->manager->flush();

        return;
    }
}
