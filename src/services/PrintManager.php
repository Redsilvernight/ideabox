<?php

namespace App\services;

use App\Entity\Idea;
use Egulias\EmailValidator\Result\Reason\ExceptionFound;
use Exception;

class PrintManager
{
    public function printIdea(Idea $idea, $ipPrinter)
    {
        $socket = $this->connectSocket($ipPrinter);
        $message = $idea->getIdeaTagId()->getTagName() . "|" . $idea->getIdeaName();
        $this->sendMessage($socket, $message);
        
    }

    public function testPrinter($ipPrinter)
    {
        $socket = $this->connectSocket(($ipPrinter));
        if($socket) {
            $this->sendMessage($socket, "ping");
            return true;
        } else {
            return false;
        }
    }
    

    private function sendMessage($socket, $message)
    {
        socket_write($socket, $message, strlen($message)) or print("Could not send data to server\n");
        socket_close($socket);
        return;
    }

    private function connectSocket($host)
    {
        $port = 1030;
        // No Timeout 
        set_time_limit(0);
        $socket = socket_create(AF_INET, SOCK_STREAM, 0) or exit("Could not create socket");
        $result = socket_connect($socket, $host, $port);
        if(!$result){
            return false;
        } else {
            return $socket;
        }
    }
}
