<?php

namespace App\services;

use App\Entity\IdeaBox;
use App\Entity\User;
use App\Repository\IdeaBoxRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

class BoxManager
{
    private $manager;
    private $tagRepository;
    private $userRepository;
    private $ideaBoxRepository;

    public function __construct(EntityManagerInterface $manager, TagRepository $tagRepository, UserRepository $userRepository, IdeaBoxRepository $ideaBoxRepository)
    {
        $this->manager = $manager;
        $this->tagRepository = $tagRepository;
        $this->userRepository = $userRepository;
        $this->ideaBoxRepository = $ideaBoxRepository;
    }

    public function createBox(User $user, $flushing = true)
    {
        $newBox = new IdeaBox();
        $newBox->setBoxName("Boîte à idée de ". $user->getUsername())
                ->setBoxCreatedAt(new DateTimeImmutable('now'))
                ->addBoxOwnerId($user)
                ->setIsPrintable(0);
        
        $this->manager->persist($newBox);

        if ($flushing) {
            $this->manager->flush();
        }
    }

    public function removeAccount($deletedBox)
    {
        $this->deleteOnly($deletedBox);
        $this->manager->flush();
    }

    public function removeBox(User $user, $force = false)
    {
        $deletedBox = $user->getIdeaBox();
        
        if ($force) {
            $this->manager->remove($deletedBox);
            $this->manager->flush();
            return;
        } else {
            $listOwner = $user->getIdeaBox()->getBoxOwnerId();
            
            if (count($listOwner) > 1) {
                foreach ($listOwner as $owner) {
                    $this->createBox($owner, false);
                }
                $this->manager->remove($this->ideaBoxRepository->findOneById($deletedBox));
                $this->manager->flush();
                
            } else {
                $this->createBox($user, false);
                $this->deleteOnly($deletedBox);
            }

            $this->manager->flush();
            return;
        }
    }

    private function deleteOnly($deletedBox)
    {
        $this->manager->remove($deletedBox);
        return;
    }


    public function listTagAvailable(User $user)
    {
        return $this->tagRepository->findBy(["tag_box_id" => $user->getIdeaBox()]);
    }

    public function listOwnerBox(User $user)
    {
        return $this->userRepository->findBy(["ideaBox" => $user->getIdeaBox()]);
    }

    public function removeOwner(User $user)
    {
        $this->createBox($user);
        return;
    }
}
