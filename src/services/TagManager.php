<?php

namespace App\services;

use App\Entity\Tag;
use App\Entity\User;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;

class TagManager
{
    private $manager;
    private $repository;

    public function __construct(EntityManagerInterface $manager, TagRepository $tagRepository)
    {
        $this->manager = $manager;
        $this->repository = $tagRepository;
    }

    public function addTag(Tag $newTag, User $user)
    {
        $newTag->setTagBoxId($user->getIdeaBox());
        
        $this->manager->persist($newTag);
        $this->manager->flush();

        return;
    }

    public function listTag(User $user)
    {
        return $this->repository->findBy(["tag_box_id" => $user->getIdeaBox()->getId()]);
    }

    public function removeTag(Tag $tag)
    {
        $this->manager->remove($tag);
        $this->manager->flush();

        return;
    }
}
