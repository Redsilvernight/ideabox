<?php

namespace App\services;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    private $userRepository;
    private $manager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
    }
    
    public function removeAccount(User $user)
    {
        $this->manager->remove($user);

        $this->manager->flush();

        return;
    }
}
