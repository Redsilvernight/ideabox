<?php

namespace App\Controller;

use App\Entity\User;
use DateTimeImmutable;
use App\Entity\IdeaBox;
use App\Entity\Invitation;
use App\Form\InvitationType;
use App\services\BoxManager;
use App\services\UserManager;
use Symfony\Component\Uid\Uuid;
use App\services\InvitationManager;
use Flasher\Prime\FlasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SettingController extends AbstractController
{
    private $boxManager;
    private $userManager;
    private $manager;
    private $flasher;

    public function __construct(BoxManager $boxManager, UserManager $userManager, EntityManagerInterface $manager, FlasherInterface $flasher)
    {
        $this->boxManager = $boxManager;
        $this->userManager = $userManager;
        $this->manager = $manager;
        $this->flasher = $flasher;
    }

    
    #[Route('/setting', name: 'app_setting')]
    #[IsGranted('ROLE_USER')]
    public function setting(): Response
    {
        return $this->render('setting/setting.html.twig', [
            'listOwnerBox' => $this->boxManager->listOwnerBox($this->getUser()),
            'ipPrinter' => $this->getUser()->getIdeaBox()->getPrinterIp()?:"null"
        ]);
    }

    
    #[Route(path: '/setting/remove/{id}', name: 'app_setting_remove')]
    #[IsGranted('ROLE_USER')]
    public function removeAccount(User $user)
    {
        $this->userManager->removeAccount($user);

        if(count($user->getIdeaBox()->getBoxOwnerId()) == 1)
        {
            $this->boxManager->removeAccount($user);
        }
        

        return $this->redirectToRoute("app_login");
    }

    
    #[Route(path: '/setting/invite', name: 'app_setting_invite')]
    #[IsGranted('ROLE_USER')]
    public function invite(Request $request, InvitationManager $invitationManager)
    {
        $newInvitation = new Invitation();

        $invitationForm = $this->createForm(InvitationType::class, $newInvitation);
        $invitationForm->handleRequest($request);

        if($invitationForm->isSubmitted())
        {
            $newInvitation->setUuid(Uuid::v4())
                        ->setAccount($this->getUser());
            $this->manager->persist($newInvitation);
            $this->manager->flush();

            $invitationManager->sendEmailInvite($newInvitation);

            $this->flasher->addSuccess('Utilisateur invité avec succés');

            return $this->redirectToRoute("app_setting");
        }

        return $this->render('setting/invite.html.twig', [
            'formInvitation' => $invitationForm->createView(),
        ]);
    }

    
    #[Route(path: '/setting/removeOwner/{owner}', name: 'app_setting_removeOwner')]
    #[IsGranted('ROLE_USER')]
    public function removeOwner(User $owner)
    {
        $this->boxManager->removeOwner($owner);
        return $this->redirectToRoute("app_setting");
                
    }

    
    #[Route(path: 'setting/link/{ipPrinter}', name: 'app_setting_link')]
    #[IsGranted('ROLE_USER')]
    public function linkPrinter($ipPrinter)
    {

        $this->getUser()->getIdeaBox()->setPrinterIp($ipPrinter);
        $this->manager->flush();

        $this->flasher->addSuccess('Imprimante liéé avec succés');
        
        return $this->redirectToRoute("app_setting");
    }

}
