<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Form\TagType;
use App\services\TagManager;
use Flasher\Prime\FlasherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TagController extends AbstractController
{
    private $tagManager;
    private $flasher;

    public function __construct(TagManager $tagManager, FlasherInterface $flasher)
    {
        $this->tagManager = $tagManager;
        $this->flasher = $flasher;
    }

    #[Route('/tag/add', name: 'tag_add')]
    #[IsGranted('ROLE_USER')]
    public function tagAdd(Request $request): Response
    {
        $tag = new Tag();
        
        $formTag = $this->createForm(TagType::class, $tag);
        $formTag->handleRequest($request);
        
        if ($formTag->isSubmitted())
        {
            $this->tagManager->addTag($tag,$this->getUser());
            $this->flasher->addSuccess('Catégorie ajoutée avec succés');
            return $this->redirectToRoute("app_box");
        }

        return $this->render('tag/add.html.twig', [
            'formTag' => $formTag->createView(),
        ]);
    }

    #[Route('/tag/list', name: 'app_tag_list')]
    #[IsGranted('ROLE_USER')]
    public function tagList()
    {
        return $this->render('tag/list.html.twig', [
            'tagInBox' => $this->tagManager->listTag($this->getUser())
        ]);
    }

    #[Route('/tag/remove/{id}', name: 'app_tag_remove')]
    #[IsGranted('ROLE_USER')]
    public function tagRemove(Tag $tag)
    {
        $this->tagManager->removeTag($tag);

        $this->flasher->addSuccess('Catégorie supprimée avec succés');

        return $this->redirectToRoute('app_tag_list');
    }
}
