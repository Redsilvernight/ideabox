<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Entity\Idea;
use App\Form\IdeaType;
use App\services\BoxManager;
use App\services\IdeaManager;
use App\services\PrintManager;
use App\Repository\IdeaRepository;
use Flasher\Prime\FlasherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IdeaController extends AbstractController
{
    private $ideaManager;
    private $printerManager;
    private $flasher;
    private $ideaRepository;

    public function __construct(IdeaManager $ideaManager, PrintManager $printerManager, FlasherInterface $flasher, IdeaRepository $ideaRepository)
    {
        $this->ideaManager = $ideaManager;
        $this->printerManager = $printerManager;
        $this->flasher = $flasher;
        $this->ideaRepository = $ideaRepository;
    }

    
    #[Route('/idea/add', name: 'app_idea_add')]
    #[IsGranted('ROLE_USER')]
    public function ideaAdd(Request $request, BoxManager $boxManager): Response
    {
        $idea = new Idea();
        $allTagInBox = $boxManager->listTagAvailable($this->getUser());

        $ideaForm = $this->createForm(IdeaType::class, $idea, [
            'available_tags' => $allTagInBox,
        ]);
        $ideaForm->handleRequest($request);

        if($ideaForm->isSubmitted())
        {
            $this->ideaManager->addIdea($idea, $this->getUser());

            if($this->getUser()->getIdeaBox()->isIsPrintable() == true && $this->getUser()->getIdeaBox()->isDefaultPrint() == true)
            {
                $this->printerManager->printIdea($idea, $this->getUser()->getIdeaBox()->getPrinterIp());
            }

            $this->flasher->addSuccess('Idée ajoutée avec succés');
            $this->redirectToRoute("app_idea_add");
        }


        
        return $this->render('idea/add.html.twig', [
            'formIdea' => $ideaForm->createView(),
        ]);
    }

    
    #[Route('/idea/list', name: 'app_idea_list')]
    public function ideaList()
    {
        return $this->render('idea/list.html.twig', [
            'ideaInBox' => $this->ideaManager->listIdea($this->getUser())
        ]);
    }

    #[Route('/idea/remove/{id}', name: 'app_idea_remove')]
    #[IsGranted('ROLE_USER')]
    public function ideaRemove(Idea $idea)
    {
        $this->ideaManager->removeIdea($idea);

        $this->flasher->addSuccess('Idée supprimée avec succés');

        return $this->redirectToRoute('app_idea_list');
    }

    
    #[Route('/idea/print/{id}', name: 'app_idea_print')]
    #[IsGranted('ROLE_USER')]
    public function ideaPrint(Idea $idea)
    {
        $this->printerManager->printIdea($idea, $this->getUser()->getIdeaBox()->getPrinterIp());
        
        return $this->redirectToRoute("app_idea_list");
    }

    
    #[Route('/idea/random/{id}', name: 'app_idea_random')]
    #[IsGranted('ROLE_USER')]
    public function randomIdea(Tag $tag): Response
    {
        $listIdea = $this->ideaRepository->findBy(["idea_tag_id" => $tag->getId()]);
        $randomKey = array_rand($listIdea,1);

        return $this->render('idea/random_idea.html.twig', [
            'random_idea' => $listIdea[$randomKey]
        ]);
    }

}
