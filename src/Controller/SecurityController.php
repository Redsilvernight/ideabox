<?php

namespace App\Controller;

use App\services\PrintManager;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $manager;
    private $flasher;

    public function __construct(EntityManagerInterface $manager, FlasherInterface $flasher)
    {
        $this->manager = $manager;
        $this->flasher = $flasher;
    }

    #[Route(path: '/', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("app_idea_add");
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route(path: '/loginCheck', name: 'app_login_checkIsVerified')]
    public function loginCheck(PrintManager $printManager)
    {
        if($this->getUser()->isVerified() != 1)
        {
            return $this->redirectToRoute("app_logout");
        }

        if($this->getUser()->getIdeaBox()->getPrinterIp() != null)
        {
            if($printManager->testPrinter($this->getUser()->getIdeaBox()->getPrinterIp()))
            {
                $this->getUser()->getIdeaBox()->setIsPrintable(1);
            }
            else{
                $this->getUser()->getIdeaBox()->setIsPrintable(0);
            }

            $this->manager->flush();
        }
        
        return $this->redirectToRoute("app_box");
    }
}
