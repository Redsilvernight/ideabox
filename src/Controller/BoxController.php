<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\BoxType;
use App\Entity\IdeaBox;
use App\services\BoxManager;
use Flasher\Prime\FlasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BoxController extends AbstractController
{
    private $manager;
    private $flasher;
    private $boxManager;

    public function __construct(EntityManagerInterface $manager, FlasherInterface $flasher, BoxManager $boxManager)
    {
        $this->manager = $manager;
        $this->flasher = $flasher;
        $this->boxManager = $boxManager;
    }

    #[Route('/box', name: 'app_box')]
    #[IsGranted('ROLE_USER')]
    public function index(): Response
    {
        return $this->render('box/box.html.twig', [
            
        ]);
    }

    
    #[Route('/box/defaultPrint/{val}', name: 'app_default_print')]
    #[IsGranted('ROLE_USER')]
    public function defaultPrint($val): Response
    {
        $this->getUser()->getIdeaBox()->setDefaultPrint($val);

        $this->manager->flush();

        return $this->redirectToRoute("app_setting");
    }

    
    #[Route('/box/rename', name: 'app_rename_box')]
    #[IsGranted('ROLE_USER')]
    public function renameBox(Request $request): Response
    {
        $newBox = new IdeaBox();
        $editForm = $this->createForm(BoxType::class, $newBox);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
                $this->getUser()->getIdeaBox()->setBoxName($newBox->getBoxName());
                $this->manager->flush();

                $this->flasher->addSuccess('Boîte renomée avec succés');
                return $this->redirectToRoute("app_setting");
        }

        return $this->render('box/box_rename.html.twig',[
            'renameForm' => $editForm->createView()
        ]);
    }

    
    #[Route('/box/delete', name: 'app_delete_box')]
    #[IsGranted('ROLE_USER')]
    public function removeBox(): Response
    {
        $this->boxManager->removeBox($this->getUser());

        return $this->redirectToRoute("app_logout");
    }
}
